#include <stdio.h>
/* Input : a, b, n */

double f(double x){
     return x*x;
}

void main(int argc, char**argv) {

  double a ,b ,h, approx, x_i;
  int i, n ;


  scanf("%lf",&a);
  scanf("%lf",&b);
  scanf("%d",&n);

  h = (b-a)/n;
  approx = (f(a) +f(b))/2.0 ;
  for (i=0;i<n-1;i++){
    x_i = a + i*h ;
    approx += f(x_i);
  }
  approx = h * approx;
  printf("approx %lf: ", approx);
  println("");

}

